/* Created by HP on 6/16/2018.*/
/* Author: Mainul Hasan.*/

// Select the canvas element in html document
const canvas = document.getElementById("myCanvas");

// Set the canvas to screen width & height
canvas.width = window.innerWidth;
canvas.height = window.innerHeight;

// Create a Drawing Object
const c = canvas.getContext('2d');

const mouse = {
    x: innerWidth / 2,
    y: innerHeight / 2
};

const colorArray = [
    '#E37B40',
    '#46B29D',
    '#DE5B49',
    '#324D5C',
    '#F0CA4D'
];

//user input
let particleNumUser = 200;
let radiusUser = 20;
//................
let radiusValue = radiusUser; //dont change this value
let particleNum = particleNumUser; //dont change this value

// decrease the radius of balls if the screen dimension is reduced
function resolveResponsive() {
    if (canvas.height <= 480) {
        particleNum = particleNumUser / 2;
    }

    if (canvas.width <= 992 && canvas.width >= 768) {
        radiusValue = radiusUser * 0.8;
    } else if (canvas.width < 768 && canvas.width >= 480) {
        radiusValue = radiusUser * 0.6;
    } else if (canvas.width < 480) {
        radiusValue = radiusUser * 0.4;
    } else {
        radiusValue = radiusUser;
    }
}

resolveResponsive();

// Event Listeners
addEventListener('mousemove', function (event) {
    mouse.x = event.clientX;
    mouse.y = event.clientY;
})

addEventListener('resize', () => {
    canvas.width = innerWidth;
    canvas.height = innerHeight;
    resolveResponsive();

    init();
})

//***************************************//
//Some useful functions
function randomIntFromRange(min, max) {
    /*
    This function will return a random integer between (min,max).
    For example: if min = 1 & max = 5, then we get a random integer between 1 & 5
    */
    return Math.floor(Math.random() * (max - min + 1) + min);
}

function randomColor(colorArray) {
    /*
    This function gets random color value from a color array.
    */
    return colorArray[Math.floor(Math.random() * colorArray.length)];
}

function distance(x1, y1, x2, y2) {
    /*
    This function computes the distance between two coordinates (x1,y1) & (x2,y2)
    */
    const xDist = x2 - x1;
    const yDist = y2 - y1;
    return Math.sqrt(Math.pow(xDist, 2) + Math.pow(yDist, 2));
}

/**

* Rotates coordinate system for velocities
 *
 * Takes velocities and alters them as if the coordinate system they're on was rotated
 *
 * @param  Object | velocity | The velocity of an individual particle
 * @param  Float  | angle    | The angle of collision between two objects in radians
 * @return Object | The altered x and y velocities after the coordinate system has been rotated
 */

function rotate(velocity, angle) {
    const rotatedVelocities = {
        x: velocity.x * Math.cos(angle) + velocity.y * Math.sin(angle),
        y: -velocity.x * Math.sin(angle) + velocity.y * Math.cos(angle)
    };

    return rotatedVelocities;
}
/** Swaps out two colliding particles ' x and y velocities after running through
 * an elastic collision reaction equation 
 *
 * @param Object | particle | A particle object with x and y coordinates, plus velocity
 * @param Object | otherParticle | A particle object with x and y coordinates, plus velocity 
 * @return Null | Does not return a value
 */

function resolveCollision(particle, otherParticle) {
    const xVelocityDiff = particle.velocity.x - otherParticle.velocity.x;
    const yVelocityDiff = particle.velocity.y - otherParticle.velocity.y;

    const xDist = otherParticle.x - particle.x;
    const yDist = otherParticle.y - particle.y;

    // Prevent accidental overlap of particles
    if (xVelocityDiff * xDist + yVelocityDiff * yDist >= 0) {

        // Grab angle between the two colliding particles
        const angle = Math.atan2(otherParticle.y - particle.y, otherParticle.x - particle.x);

        // Store mass in var for better readability in collision equation
        const m1 = particle.mass;
        const m2 = otherParticle.mass;

        // Velocity before equation
        const u1 = rotate(particle.velocity, angle);
        const u2 = rotate(otherParticle.velocity, angle);

        // Velocity after 1d collision equation
        const v1 = {
            x: u1.x * (m1 - m2) / (m1 + m2) + u2.x * 2 * m2 / (m1 + m2),
            y: u1.y
        };
        const v2 = {
            x: u2.x * (m1 - m2) / (m1 + m2) + u1.x * 2 * m2 / (m1 + m2),

            y: u2.y
        };

        // Final velocity after rotating axis back to original location
        const vFinal1 = rotate(v1, -angle);
        const vFinal2 = rotate(v2, -angle);

        // Swap particle velocities for realistic bounce effect
        particle.velocity.x = vFinal1.x;
        particle.velocity.y = vFinal1.y;

        otherParticle.velocity.x = vFinal2.x;
        otherParticle.velocity.y = vFinal2.y;
    }
}

//***************************************//

// Objects
function Particle(x, y, radius) {
    this.x = x;
    this.y = y;
    this.velocity = {
        x: (Math.random() - 0.5) * 5,
        y: (Math.random() - 0.5) * 5
    };
    this.radius = radius;
    this.color = randomColor(colorArray);
    this.mass = 1;
    this.opacity = 0;

    this.update = function (particles) {
        this.draw();

        // detect if the particles are colliding & if true, make them bounce of each other
        for (let i = 0; i < particles.length; i++) {
            if (this === particles[i]) continue;
            if (distance(this.x, this.y, particles[i].x, particles[i].y) < this.radius * 2) {
                resolveCollision(this, particles[i]); // call the resolve collision function
            }
        }

        // make the particles bounce off edges
        if (this.x + this.radius > canvas.width || this.x - this.radius <= 0) {
            this.velocity.x = -this.velocity.x;
        }

        if (this.y + this.radius > canvas.height || this.y - this.radius <= 0) {
            this.velocity.y = -this.velocity.y;
        }

        // detect mouse collision with particles and change their color, otherwise reset their fill opacity
        if (distance(mouse.x, mouse.y, this.x, this.y) < 120 && this.opacity < 0.5) {
            this.opacity += 0.02;
        } else if (this.opacity > 0) {
            this.opacity -= 0.02;
            this.opacity = Math.max(0, this.opacity); // ensure fill opacity does not get exactly 0
        }

        this.x += this.velocity.x;
        this.y += this.velocity.y;
    }

    // the main drawing function
    this.draw = function () {
        c.beginPath();
        c.arc(this.x, this.y, this.radius, 0, Math.PI * 2, false);
        c.save();
        c.globalAlpha = this.opacity;
        c.fillStyle = this.color;
        c.fill();
        c.restore();
        c.strokeStyle = this.color;
        c.stroke();
        c.closePath();
    }
}

// Implementation
let particles;

function init() {
    particles = [];

    for (let i = 0; i < particleNum; i++) {
        let radius = radiusValue;
        let x = randomIntFromRange(radius, canvas.width - radius);
        let y = randomIntFromRange(radius, canvas.height - radius);
        // ensure that particles are not overlapping each other
        if (i !== 0) {
            for (let j = 0; j < particles.length; j++) {
                if (distance(x, y, particles[j].x, particles[j].y) < radius * 2) {
                    //do something
                    x = randomIntFromRange(radius, canvas.width - radius);
                    y = randomIntFromRange(radius, canvas.height - radius);
                    j = -1;
                }
            }
        }
        particles.push(new Particle(x, y, radius));
    }
}

// Animation Loop
function animate() {
    requestAnimationFrame(animate);
    c.clearRect(0, 0, canvas.width, canvas.height);
    /*
    The forEach() method calls a provided function once for each element in an array, in order.
      * Syntax : array.forEach(function(currentValue, index, arr), thisValue)
      * Example:
          var a = ["a", "b", "c"];
          a.forEach(function(entry) {
          console.log(entry);//prints out each values one by one
          });
    */
    particles.forEach(function (item) {
        item.update(particles);
    });
    //ECMA6 Version
    /*particles.forEach(particles=> {
        particles.update();
    });*/
}

init();
animate();
